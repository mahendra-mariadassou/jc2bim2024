%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
\subsection{Random graph models}
%-------------------------------------------------------------------------------

%==================================================================
\frame{ \frametitle{Degree-based models}

  \bigskip 
  \textbf{Aim:} account for heterogeneous number of neighbors $D_i = \sum_{j \neq i} Y_{ij}$
  
  \bigskip \bigskip \pause
  \hspace{-.04\textwidth}
  \begin{tabular}{p{.45\textwidth}p{.45\textwidth}}  
    \begin{tabular}{p{.45\textwidth}}
      \textbf{Expected degree distribution (EDD) \refer{ChL02}:} \\
      $$
      p_{ij} := \Pr\{i \sim j\} = d_i d_j / \lambda 
      $$
      $d_i =$ observed degree of node $i$, \\ ~ \\
      $\lambda = \sum_i d_i 
      \quad \Rightarrow \quad 
      \emphase{\Esp D_i = d_i}$ \\ ~ \\
      ($p_{ij}$ may exceed 1...) \\ ~ \\ 
      ~ \\ 
      \onslide+<3>{
      \textbf{Configuration model} (looks similar but actually different): requires
      $$
      D_i = d_i
      $$
      }
    \end{tabular}
    &
    \hspace{-.05\textwidth}
    \begin{tabular}{p{.45\textwidth}}
      \includegraphics[width=.5\textwidth]{\figeco/florida-EDD-simul.pdf}
    \end{tabular} 
  \end{tabular}

% }

%==================================================================
\frame{ \frametitle{EDD for oriented graphs}

  \bigskip
  \textbf{Degrees:} out-degree $D^+_i = \sum_j Y_{ij}$, in-degree  $D^-_i = \sum_j Y_{ji}$

  \bigskip \bigskip \pause
  \hspace{-.04\textwidth}
  \begin{tabular}{p{.45\textwidth}p{.45\textwidth}}  
    \begin{tabular}{p{.4\textwidth}}
      Connexion probability
      $$
      p_{ij} := \Pr\{i \rightarrow j\} = d^+_i d^-_j / \lambda 
      $$
      \bigskip
      Expected degrees:
      $$
      \Esp D^+_i = d^+_i, 
      \quad 
      \Esp D^-_i = d^-_i
      $$ 
      
      \bigskip
      Only accounts for 'generalists' vs 'specialists' (see top left node) \\
      \bigskip
      \onslide+<3>{
      \textbf{Again:} similar but different from 'edge rewiring', which imposes
      $$
      D^+_i = d^+_i, \quad D^-_i = d^-_i
      $$}
    \end{tabular}
    &
    \hspace{-.05\textwidth}
    \begin{tabular}{p{.45\textwidth}}
      \includegraphics[width=.5\textwidth]{\figeco/foodweb-baydry-EDD-simul.pdf}
    \end{tabular} 
  \end{tabular}

}

%==================================================================
\frame{ \frametitle{Latent-space models}

  \textbf{Fact:} observed networks are far from 'random' or 'uniform' (i.e. Erd\"os )
  
  \bigskip \bigskip 
  \textbf{Rational:} the observed heterogeneity is due to (unobserved) node specificities

  \bigskip \bigskip \pause
  \textbf{General framework:} latent space models \refer{BJR07,MaR14}
  \begin{itemize}
   \item A latent (= hidden = unobserved) variable $Z_i$ is associated with each node
   \item The connections are independent conditionally on $Z = \{Z_i\}$:
   $$
   \{Y_{ij}\} \text{ indep. } \mid Z:
   \qquad 
   P(Y_{ij} = 1) = \gamma(Z_i, Z_j)
   $$
  \end{itemize}
  
  \bigskip \bigskip \pause
  \textbf{Exchangeable graphs:} provided that the $Z_i$'s are iid, for any permutation $\sigma$,
  $$
  p(\{Y_{ij}\}) = p(\{Y_{\emphase{\sigma}(i)\emphase{\sigma}(j)}\})
  $$

}

%==================================================================
\frame{ \frametitle{Latent positions}

  \textbf{Latent position model \refer{HRH02}.} $Z_i \in \Rbb^d$,
  $$
  \log \frac{p_{ij}}{1 - p_{ij}} = \alpha - \|Z_i-Z_j\|
  $$

  \bigskip \pause
  \begin{tabular}{p{.45\textwidth}p{.45\textwidth}}
%     \hspace{-.1\textwidth}
    \begin{tabular}{p{.45\textwidth}}
      Latent positions: \\
      \includegraphics[width=.4\textwidth]{\fignet/LatentPositionModel-Network}
    \end{tabular} 
    &
%     \hspace{-.1\textwidth}
    \begin{tabular}{p{.45\textwidth}}
      Observed data: \\
      \includegraphics[width=.4\textwidth]{\fignet/LatentPositionModel-Adjacency}
    \end{tabular} 
  \end{tabular}
  
  \pause
  \textbf{Clustering version:} \refer{HRT07}

}

%==================================================================
\frame{ \frametitle{Statistical inference for stochastic blockmodels}

  \textbf{Exercise:} draw the graphical model of latent variable model for graph
  
  \bigskip
  \begin{tabular}{p{.6\textwidth}p{.4\textwidth}}
    \hspace{-0.04\textwidth}
    \begin{tabular}{p{.5\textwidth}}
      \onslide+<2->{\textbf{Solution} for $p(Y, Z)$ ~\\}
      \bigskip
      \onslide+<3->{
        \textbf{Incomplete data model:} $Z$ is not observed \\ ~\\
        \ra Standard statistical inference (e.g. EM algorithm) requires to compute \emphase{$p(Z \mid Y)$} ~\\
        }
      \bigskip
      \onslide+<4->{
        \textbf{Graph moralization:} the dependency structure of $p(Z \mid Y)$ is (very) intricate \\ ~\\
        \ra Need to resort to Monte-Carlo sampling \refer{NoS01,MSP05} or variational approximations \refer{GoN05,DPR08}
        }
    \end{tabular}
    &
    \begin{tabular}{p{.4\textwidth}}
      \begin{overprint}
        \onslide<2-3>
%           \begin{centering}
            % \renewcommand{\nodesize}{1.5em}
            % \input{SBM-GraphModel-pZY} 
%           \end{centering}       
        \onslide<4>
%           \begin{centering}
            % \renewcommand{\nodesize}{1.5em}
            % \input{SBM-GraphModel-pZmY} 
%           \end{centering}       
      \end{overprint}
    \end{tabular} 
  \end{tabular}

}
