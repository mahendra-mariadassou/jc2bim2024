---
title: JC2BIMMM 2024
---

# Photo de groupe

![Photo de groupe](imgs/group_photo.png)

# Programme

![Programme de l'école](imgs/programme.png)

# Supports de cours de l'école [JC2BIM 2024](https://www.gdr-bim.cnrs.fr/ecole-jc2bim/). 

[Lien vers le Gitlab](https://forgemia.inra.fr/mahendra-mariadassou/jc2bim2024/-/tree/main?ref_type=heads)

#### Informations diverses

* [Mot d'ouverture](https://docs.google.com/presentation/d/1aDETOPqEeEm-RTfFAeYZAImrIx7yGZcq-rvK2iz1yew/edit?usp=sharing)

## Algorithmique des séquences

#### Cours et TP


* Alignement de séquences : [supports [pdf]](AlgoSeq/Cours/AlgoSeq2024_SequenceAlignment_Lemaitre.pdf)
* Indexation plein texte: [supports [pdf]](AlgoSeq/Cours/AlgoSeq2024_Indexing_Salson.pdf) ([recherche par dichotomie [html]](AlgoSeq/Cours/AlgoSeq2024_Indexing_Salson))
* TP sur le *read mapping* : [supports [pdf]](AlgoSeq/Cours/AlgoSeq2024_TPAlignment_Salson.pdf)
* Assemblage de génomes : [supports [pdf]](AlgoSeq/Cours/AlgoSeq2024_GenomeAssembly_Lemaitre.pdf)
* TP assemblage de génomes : [supports [pdf]](AlgoSeq/Ateliers/TP_assemblage_Lemaitre/AlgoSeq2024_TP_assemblage.pdf), [matériel et notebook](https://forgemia.inra.fr/mahendra-mariadassou/jc2bim2024/-/tree/main/AlgoSeq/Ateliers/TP_assemblage_Lemaitre?ref_type=heads)
* Cours sur le sketching (échantillonnage) : [supports [pdf]](AlgoSeq/Cours/AlgoSeq2024_Sketching_Marchet.pdf)

<!--
* Indexation plein texte: [supports [pdf]](https://mahendra-mariadassou.pages.mia.inra.fr/jc2bim2024/AlgoSeq/Cours/algoseq2024-indexation-salson.pdf)
* Assemblage de génomes: [supports [pdf]](https://mahendra-mariadassou.pages.mia.inra.fr/jc2bim2024/AlgoSeq/Cours/algoseq2024_assemblage_lemaitre.pdf)
* Alignment-free : [supports [pdf]](https://mahendra-mariadassou.pages.mia.inra.fr/jc2bim2024/AlgoSeq/Cours/algoseq2024_alignment-free_salson_marchet.pdf)
-->

#### Ateliers

* Bases de la programmation dynamique pour l'alignement : [support [pdf]](AlgoSeq/Ateliers/atelier_alignement_Touzet.pdf)
* Algorithmes pour le RNA-Seq : [supports [pdf]](AlgoSeq/Ateliers/Atelier_RNA-seq_Marchet.pdf)
* Sketching : [support [github]](https://github.com/yoann-dufresne/JC2BIMMM_sketches/tree/main)
* Atelier pangénome : [support [pdf]](AlgoSeq/Ateliers/atelier_pangenome/atelier_pang_short.pdf), [fichiers pour le TP](https://forgemia.inra.fr/mahendra-mariadassou/jc2bim2024/-/tree/main/AlgoSeq/Ateliers/atelier_pangenome)
* Atelier BLAST : [énoncé [pdf]](AlgoSeq/Ateliers/atelier_blast_Touzet_Mariadassou.pdf)
* Atelier structure de données pour les k-mers : [support [pdf]](AlgoSeq/Ateliers/Atelier_k-mer_structures_Marchet.pdf)
<!--
* Alignement de reads avec Bowtie2 [énoncé [pdf]](https://mahendra-mariadassou.pages.mia.inra.fr/jc2bim2024/AlgoSeq/Ateliers/Bowtie/algoseq2024-bowtie.pdf) et [Snakefile (pour l'ex. 2)](https://forgemia.inra.fr/guillem.rigaill/slides_jc2bim/-/blob/main/AlgoSeq/Ateliers/Bowtie/Snakefile). Les diapos sur Bowtie2 sont dans le cours sur l'indexation.

-->

## Statistiques

#### Cours et TP

* Statistiques inférentielles: [supports [html]](Stat/Cours/Stat_inferentielle/stats2024_StatsInferentielles_Mariadassou.html)
* Statistiques inférentielles (TP): [instructions d'installation](http://forgemia.inra.fr/mahendra-mariadassou/jc2bim2024/-/tree/main/Stat/Cours/Stat_inferentielle_TP)
* Analyse différentielle: [supports [pdf]](Stat/Cours/stat2024_AnalyseDifferentielle_Martin.pdf)
* Analyses de réseaux: [supports [pdf]](Stat/Cours/Stat2024_Network_Mariadassou.pdf)
* Analyse de réseaux (TP): [supports [html]](Stat/Ateliers/TP_network/TP_network.html), [fonctions R](https://mahendra-mariadassou.pages.mia.inra.fr/jc2bim2024/Stat/Ateliers/TP_network/external_functions.R), [données du TP [zip]](Stat/Ateliers/TP_network/data_school.zip) 
* Statistiques Bayésiennes: [supports [pdf]](Stat/Cours/Stat2024_Bayes_KonKamKing.pdf)
* Auto-encodeurs variationnels pour les séquences [supports [pdf]](Stat/Cours/Stat2024_AIOmics_LeCunff.pdf)

#### Ateliers

<!--
* Corrélation : [supports [pdf]](Stat/Ateliers/Stat2024_AtelierCorrelation_MarieLaureMartinMagniette_GuillemRigaill.pdf)
* Tests multiples : [supports [pdf]](Stat/Ateliers/stat2024_atelierTestMultiple_MahendraMariadassouGuillemRigaill.pdf)
* DicoExpress : [supports [pdf]](Stat/Ateliers/Stat2024_AtelierDiCoExpress_Martin.pdf)
-->
* Segmentation et détection de ruptures : [supports [pdf]](Stat/Ateliers/Segmentation/Stat2024_AtelierSegmentation_Rigaill-Liehrmann.pdf), [Code de la vignette [R]](Stat/Ateliers/Segmentation/main_demo.R), [métadonnées des fichiers .bam [txt]](Stat/Ateliers/Segmentation/sample_info_demo.txt)
* Segmentation le long du génome : [supports [html]](Stat/Ateliers/Segmentation/Stat2024_AtelierSegmentation_GuillaumeKONKAMKING.html)
* Tests multiples : [supports [html]](Stat/Ateliers/Multiple_tests/stats2024_MultipleTests_Mariadassou.html),  [code du TP [R]](Stat/Ateliers/Multiple_tests/stats2024_MultipleTestsTP_Mariadassou.R) 

## Frugalité numérique
[support (pdf)](Ouverture/Conf_Sobriete_numerique_Frenoux.pdf) de la conférence donnée par Emmanuelle Frenoux du [GDS EcoInfo](https://ecoinfo.cnrs.fr/).

# Liste des ateliers 

### Lundi 16h-17h30

ロ STAT Basique: Stat inférentielle (Mahendra Mariadassou)

Ce TP propose de mettre en oeuvre et d'approfondir les notions
présentées dans le cours du lundi matin. Le TP prend la forme de
vignette en R avec des exercices de code à compléter (voir instructions
d'installation).

### Jeudi 9h-10h30

ロ ALGOSEQ avancé : TP Assemblage (Claire Lemaitre)

Ce TP propose de mettre en œuvre les notions sur les k-mers et les
graphes de De Bruijn présentées dans le cours du mercredi. On explorera
plusieurs "petits" jeux de données de séquençage par l'analyse de leurs
k-mers en utilisant le langage Python avec des structures de données
très basiques (typiquement un dictionnaire). Le TP prend la forme d'un
notebook Jupyter avec du code à compléter.

ロ STAT avancé : Processus le long du génome (Guillaume Kon Kam King)

This practical follows the lectures on Bayesian Inference and Segmentation models. We will tackle a DNA segmentation problem, where we will decompose a DNA sequence into several homogeneous regions using an elegant approach based on hidden Markov Models. You will be guided through the construction and the implementation of a Bayesian inference framework to identify the segmentation and a DNA sequence generative model, along with uncertainty quantification. This practical will give you the occasion of applying the tools and concepts presented the Bayesian Inference course on an interesting problem, it will allow you to build a deeper understanding and develop your first hands-on experience of prior specification and posterior sampling. Solutions will be provided as R code, but you can consider using your preferred programming language or pseudo-code.

### Jeudi 14h30-16h00

ロ STAT Basique: Analyse de réseaux (Mahendra Mariadassou et Marie-Laure
Martin)

Cet atelier proposera de découvrir tous les indicateurs qui permettent
de décrire un réseau à partir d'un TP sous R. Nous distinguerons les
indicateurs globaux des locaux et nous verrons quelles sont les valeurs
habituellement vues dans les réseaux moléculaires.

ロ ALGOSEQ Basique: Algos pour le RNA-Seq (Camille Marchet)

Nous ferons un tour d\'horizon des différentes structures de données qui
permettent d\'indexer des jeux de données ou des génomes à grande
échelle par le biais de leurs k-mers.

### Jeudi 16h30-18h00



ロ ALGOSEQ Basique: Exercices sur les bases de la programmation dynamique pour l'alignement (Hélène
Touzet)

Cet atelier proposera des exercices de pratique "sur papier" autour
de la programmation dynamique, et son application à l'alignement de
séquences.

ロ ALGOSEQ Avancé: Sketching - comparaison rapide de génomes (Yoann
Dufresne)

Nous mettrons en oeuvre différentes méthodes de sketching vues lors du
cours de Camille Marchet afin de montrer expérimentalement la pertinence
de la comparaison de génomes par cette technique. Différents algorithmes
seront implémentés en Python avec pour but de voir les impacts des
décisions d\'échantillonnage dans le résultat final.

### Vendredi 11h-12h30

ロ ALGOSEQ Basique : Graphes de pangénomes (Benjamin Linard)

Cet atelier introduira les concepts et outils derrière le "graphe de
variation", un modèle pour intégrer la diversité des variations
génomiques caractérisant une espèce. On abordera la construction, la
visualisation, le mapping de requêtes sur le graphe puis nous étudierons
un cas concret de délétion ayant un impact phénotypique chez un arbre
fruitier.

ロ STAT avancé : Tests multiples (Mahendra Mariadassou)

Cet atelier sera consacré à la problématique des tests multiples et de
l'inférence post-hoc. Elle approfondira la thématique des tests
multiples vue dans les cours d'inférence de réseaux et d'analyse
différentielle et présentera des outils en ligne permettant de faire de
l'inférence post-hoc dans le contexte de l'analyse différentielle.

### Vendredi 14h-15h30

ロ ALGOSEQ Basique: BLAST, algo et pratique (Hélène Touzet et Mahendra
Mariadassou)

Cet atelier sera consacré à des rappels et approfondissements autour de
la recherche d'homologie avec des approches "seed-and-extend", illustrés
par l'usage de BLAST (paramètres de BLAST, P-valeur etc.). Nous
présenterons également les graines espacées, qui mises en oeuvre dans
Megablast.

ロ ALGOSEQ avancé : Structures pour les k-mers (Camille Marchet)

Cet atelier fait un recensement de la bibliographie des structures de
données de k-mers. On démarre avec les structures permettant de
représenter/d'indexer un ensemble de k-mers (méthodes basées sur les
graphes de de Bruijn, sur le hachage, ou autres), avant de traiter
l'état de l'art plus récent des ensembles d'ensembles de k-mers (graphes
de de Bruijn colorés et travaux assimilés).
