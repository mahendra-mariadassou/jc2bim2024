# TP assemblage de novo avec un dictionnaire Python 

> JC2BIMMM juin 2024 --
> Claire Lemaitre

### Dictionnaire pour stocker le graphe de De Bruijn (= les kmers)
**Q1.** Ecrire une fonction `fill_dbg(fasta_file, k)` qui remplit un dictionnaire avec les kmers des séquences présentes dans un fichier fasta donné en entrée. Les clés du dictionnaire sont les kmers distincts et la valeur associée à chaque kmer est son nombre d'occurrences dans les lectures du fichier.


```python
def fill_dbg(fasta_file, k):
    dbg = {}
    with open(fasta_file,"r") as f:
        for line in f:
            line = line.strip()
            if not line.startswith('>'):
                ...
    return dbg
```

**Q2.** Appliquer cette fonction sur le fichier contenant le génome de référence du virus sars-cov-2 (fichier `covid_ref_genome.fa`). Indiquer le nombre de kmers totaux et le nombre de kmers distincts.


```python
d31 = fill_dbg("covid_ref_genome.fasta", 31)
```


```python
def stats_dbg(dbg):
    ...
```


```python
stats_dbg(d31)
```

### Histogramme des kmers (ou profil d'abondance des kmers)
**Q3.** Calculer l'histogramme des kmers : écrire une fonction `kmer_histo(dbg, ab_max)` qui renvoie une liste `histo` de taille `ab_max` telle que le `histo[i]` est le nombre de kmers distincts ayant l'abondance `i`.


```python
def kmer_histo(dbg, ab_max = 100):
    histo = [0 for i in range(ab_max)]
    for kmer in dbg:
        ...
    return histo
```

**Q4.** Appliquer cette fonction à plusieurs ensembles de kmers calculés sur le génome de référence du sars-cov-2 avec différentes valeurs de $k$ (utiliser `ab_max = 20`). A partir de quelle valeur de $k$, les kmers sont-ils tous (ou presque tous) uniques dans le génome ?


```python
kmer_histo(d31,20)
```


```python
d15 = fill_dbg("covid_ref_genome.fasta", 15)
kmer_histo(d15,20)
```


```python
d51 = fill_dbg("covid_ref_genome.fasta", 51)
kmer_histo(d51,20) 
```

### Assemblage : construction d'unitigs
**Q5.** On construira un unitig à partir d'un kmer arbitraire du graphe, qu'on va étendre vers la droite. Récupérer un kmer arbitraire du graphe à l'aide de la fonction `keys()`.


```python
...
```

**Q6.** Implémenter les fonctions `left_neighbors(kmer, dbg)` et `right_neighbors(kmer, dbg)` qui renvoient une liste des kmers voisins à gauche et à droite respectivement d'un kmer donné du graphe de De Bruin.

*Astuce : il faudra d'abord identifier quels sont les kmers voisins possibles, puis interroger leur existence dans le dictionnaire.*


```python
def right_neighbors(kmer, dbg):
    ...
```


```python
def left_neighbors(kmer, dbg):
    ...
```

**Q7.** Ecrire la fonction `right_unitig(kmer, dbg)` qui renvoie la séquence de l’extension à droite sans branchement (unitig) d’un k-mer donné (incluant le k-mer).


```python
def right_unitig(kmer,dbg):
    ...
```

**Q8.** Appliquer cette fonction à un kmer arbitraire du graphe. Quelle taille d'unitig arrive-t-on à reconstruire avec les kmers du génome de référence ?


```python
unitig = right_unitig(kmer,d31)
```


```python
len(unitig)
```

### Tests sur un vrai jeu de données de séquençage du Sars-Cov-2
Le fichier fasta `SRX9435498_subset10000.fasta` contient un sous-ensemble de 10000 lectures du jeu de données [SRX9435498](https://www.ncbi.nlm.nih.gov/sra/?term=SRX9435498) disponible dans SRA. Il s'agit d'un séquençage Illumina par capture amplicon du sars-cov-2 depuis un prélèvement chez un patient américain (nouveau mexique) lors de la pandémie en 2020.

**Q9.** Créer le graphe de De Bruijn pour ce fichier avec `k=31`, et indiquer le nombre de kmers totaux et distincts. Comparer le nombre de kmers distincts avec celui du génome de référence et expliquer cette différence. Calculer l'histogramme des kmers. Que remarque-t-on par rapport à l'histogramme calculé sur le génome de référence ?


```python
real31 = fill_dbg("SRX9435498_subset10000.fasta", 31)
stats_dbg(real31)
```


```python
h = kmer_histo(real31,100)
```

**Q10.** Représenter graphiquement cet histogramme, par exemple la fonction `pyplot` du package `matplotlib`, ou alors en utilisant R.


```python
import matplotlib.pyplot as plt
plt.figure()

# Tracé des données
plt.plot(range(100), h, marker='o', linestyle='-')
plt.yscale('log')
# Ajout des titres et des étiquettes
plt.title("Histo d'abondance des kmers")
plt.xlabel('abondance')
plt.ylabel('nb de kmers distincts')

# Affichage de la figure
plt.show()

```

**Q11.** Nous allons tenter de construire un unitig à partir d'un kmer de départ. Proposer une fonction qui permet de choisir un kmer plus judicieux que le premier de la liste.

*Astuce : utiliser l'information d'abondance...*


```python
def pick_starter(dbg, abundance):
    ...
```


```python
starter = pick_starter(real31, 20)
```

**Q12.** Calculer l'extension en unitig vers la droite pour ce kmer. Quelle taille d'unitig obtient-on ?


```python
len(right_unitig(starter,real31))
```

## Nettoyer le graphe des kmers erronés
**Q13.** Ecrire une fonction `remove_low_abundance(dbg, min_ab)` qui élimine du dictionnaire les kmers dont l'abondance est inférieure au seuil `min_ab`.


```python
def remove_low_abundance(dbg, min_ab):
    ...
```

**Q14.** Appliquer la fonction avec le seuil = 2 et recalculer les statistiques de nombre de kmers totaux et distincts. Est-ce que le nombre de kmers distincts vous parait cohérent avec la taille du génome du sars-cov-2 ?


```python
real31_clean = remove_low_abundance(real31, 2)
stats_dbg(real31_clean)
```

**Q15.** Re-calculer l'extension en unitig du kmer `starter` précédent avec ce nouveau graphe filtré. A-t-on augmenté la taille de l'extension ? 


```python
len(right_unitig(starter,real31_clean))
```

**Q16.** Ré-essayer avec un autre kmer starter, ou avec une plus petite valeur de $k$.  


```python
...
```

**Q17.** Tester avec un jeu contenant 3x plus de lectures, `SRX9435498_subset30000.fasta`.


```python
...
```

## Nettoyer encore plus le graphe : enlever les tips et les bulles
On commence par enlever les tips : unitigs de taille $<k$ (en nombre de noeuds) qui n'ont pas de successeur. On enlèvera après les bulles de taille exactement $k$ (en nombre de noeuds).

**Q18.** Ecrire une fonction `remove_tips(dbg)` qui renvoie un nouveau dbg dans lequel les noeuds des tips ont été enlevés. Pour cela, on parcourt tous les noeuds du graphe ayant exactement 2 successeurs. Pour ces noeuds, la fonction calcule les deux extensions possibles vers la droite, et si l'une est de taille $<k$ (en nombre de noeuds), alors la fonction élimine du graphe tous les noeuds qui composent cette extension. 


```python
# re-write the right-unitig function in order to get the info why the unitig stopped
def right_unitig2(kmer, dbg):
    ...

def remove_tips(dbg):
    ...
```

**Q19.** Ecrire une fonction `reduce_bubbles(dbg)` qui renvoie un nouveau dbg dans lequel les noeuds d'un des deux chemins de chaque bulle de taille $k$ ont été enlevés. Pour cela, on parcourt tous les noeuds du graphe ayant exactement 2 successeurs. Pour ces noeuds, la fonction calcule les deux extensions possibles vers la droite, et si les deux sont de taille $k$, alors la fonction élimine du graphe tous les noeuds qui composent l'une des deux extensions. 


```python
def reduce_bubbles(dbg):
    ...
```

**Q20.** Appliquer ces focntions sur les jeux de données réelles et observer si cela améliore la taille des unitigs qu'on peut construire.


```python
...
```

## Prendre en compte le reverse-complément
Pour une lecture donnée, on va considérer qu'on la "voit" à la fois en forward et en reverse-complément et un noeud du graphe représente les deux versions du kmer vu dans la lecture. On utilise sa représentation "canonique" : le plus petit mot au sens lexicographique entre le kmer et son reverse complement.

**Q21.** Implémenter la fonction `rev_comp(sequence)` qui renvoie le reverse-complément d'une séquence d'ADN, ainsi que la fonction `canonical(sequence)`  qui renvoie la version canonique d'une séquence d'ADN.


```python
def rev_comp(sequence):
    baseComplement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'} 
    S=''.join([baseComplement[base] for base in reversed(list(sequence))])
    return S
def canonical(sequence):
    ...
```


```python
canonical("ATTCGT")
```

**Q22.** Ré-implémenter les fonctions qui construisent le graphe et calculent les voisins pour prendre en compte les kmers canoniques. Et appliquer sur les jeux de données pour observer si cela améliore la taille des unitigs.


```python
...
```

**Q23.** Ecrire une fonction qui calcule tous les unitigs du jeu de données. 

*Astuce : On peut ré-utiliser la fonction qui calcule l'extension en unitig vers la droites, l'appliquer vers la gauche pour avoir un unitig complet. Il faudra parcourir les kmers du graphes pour choisir ses kmers de départ et surtout faire en sorte d'éviter de ré-assembler des kmers déjà assemblés...*


```python
...
```
